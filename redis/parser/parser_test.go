package parser

import (
	"bufio"
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseBulkString(t *testing.T) {

	expectedArray := []string{
		"$2\r\nab\r\n",
		"$4\r\na\r\nb\r\n",
		"$3\r\nabc\r\n",
	}

	for i := 0; i < len(expectedArray); i++ {
		expected := expectedArray[i]
		buf := bytes.NewBufferString(expected[4:])
		reader := bufio.NewReader(buf)
		header := []byte(expected[:2])
		ch := make(chan *Payload, 1)
		parseBulkString(header, reader, ch)

		payload := <-ch

		assert.Equal(t, expected, string(payload.Data.ToBytes()))
	}

}

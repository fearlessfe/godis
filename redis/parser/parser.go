package parser

import (
	"bufio"
	"errors"
	"io"
	"strconv"

	"gitlab.com/fearlessfe/godis/interface/redis"
	"gitlab.com/fearlessfe/godis/redis/protocol"
)

type Payload struct {
	Data redis.Reply
	Err  error
}

func parseBulkString(header []byte, reader *bufio.Reader, ch chan<- *Payload) error {
	strLen, err := strconv.ParseInt(string(header[1:]), 10, 64)
	if err != nil || strLen < -1 {
		protocolError(ch, "illegal bulk string header: "+string(header))
		return nil
	} else if strLen == -1 {
		ch <- &Payload{
			Data: protocol.MakeNullBulkReply(),
		}
		return nil
	}
	body := make([]byte, strLen+2) // strlen + \r + \n
	_, err = io.ReadFull(reader, body)
	if err != nil {
		return err
	}
	ch <- &Payload{
		Data: protocol.MakeBulkReply(body[:len(body)-2]),
	}
	return nil
}

func parseArray(header []byte, reader *bufio.Reader, ch chan<- *Payload) error {
	nStrs, err := strconv.ParseInt(string(header[1:]), 10, 64)
	if err != nil || nStrs < 0 {
		protocolError(ch, "illegal array header "+string(header[1:]))
		return nil
	} else if nStrs == 0 {
		ch <- &Payload{
			Data: protocol.MakeEmptyMultiBulkReply(),
		}
		return nil
	}

	// lines := make([][]byte, 0, nStrs)

	// for i := int64(0); i <nStrs; i++ {
	// 	var line []byte
	// 	line, err := reader.ReadBytes('\n')
	// 	if err != nil {
	// 		return err
	// 	}
	// 	length := len(line)
	// }

	return nil
}

func protocolError(ch chan<- *Payload, msg string) {
	err := errors.New("protocol error: " + msg)
	ch <- &Payload{Err: err}
}

package asserts

import (
	"fmt"
	"runtime"
	"testing"

	"gitlab.com/fearlessfe/godis/interface/redis"
	"gitlab.com/fearlessfe/godis/redis/protocol"
)

// AssertIntReply checks if the given redis.Reply is the expected integer
func AssertIntReply(t *testing.T, actual redis.Reply, expected int) {
	intResult, ok := actual.(*protocol.IntReply)
	if !ok {
		t.Errorf("expected int protocol, actually %s, %s", actual.ToBytes(), printStack())
		return
	}
	if intResult.Code != int64(expected) {
		t.Errorf("expected %d, actually %d, %s", expected, intResult.Code, printStack())
	}
}

func AssertIntReplyGreaterThan(t *testing.T, actual redis.Reply, expected int) {
	intResult, ok := actual.(*protocol.IntReply)
	if !ok {
		t.Errorf("expected int protocol, actually %s, %s", actual.ToBytes(), printStack())
		return
	}
	if intResult.Code < int64(expected) {
		t.Errorf("expected %d, actually %d, %s", expected, intResult.Code, printStack())
	}
}
func printStack() string {
	_, file, no, ok := runtime.Caller(2)
	if ok {
		return fmt.Sprintf("at %s:%d", file, no)
	}
	return ""
}

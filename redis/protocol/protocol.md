## RESP protocol spec

Redis serialization protocol (RESP) specification

### 网络层

redis 客户端使用 TCP 连接与服务端交互

### 请求-响应模型

服务端接受指令，执行指令然后返回给客户端。除了以下两种情况

1. redis 支持 pipelining,客户端可以一次发送多条指令
2. 客户端订阅 Pub/Sub 后，协议切换成 push ptotocol,客户端不需要发送指令

### 协议描述

RESP 支持以下数据类型，第一个 byte 决定数据类型

* Simple Strings: 简单字符串，以 “+” 开始
* Errors：错误， 以 “-” 开始
* Integers：整数，以 “:” 开始
* Bulk Strings: 批量字符串，以“$”开始
* Arrays: 数组，以“*”开始

RESP 在请求-响应模型中的作用

1. 客户端使用 Bulk Strings Array 发送指令
2. 服务端根据执行结果回复特点的 RESP 类型

```
// Simple Strings
"+OK\r\n"

// RESP Errors
"-Error message\r\n"
"-ERR unknown command 'helloworld'"

// RESP Integers
":0\r\n"
":1000\r\n"

// RESP Bulk Strings
// $字符长度 \r\n 内容 \r\n
"$5\r\nhello\r\n"

// RESP Arrays
// 以 * 开头，后面是数组元素个数， \r\n 后就是具体的数组项，数组项是具体的类型
"*2\r\n$5\r\nhello\r\n$5\r\nworld\r\n"

// null array
"*-1\r\n"

// 数组中也可以存在空值
*3\r\n
$5\r\n
hello\r\n
$-1\r\n
$5\r\n
world\r\n
```


### Redis pipeline

一次请求发送多条命令：可以保证命令的原子性。服务端也会按照顺序返回执行结果


